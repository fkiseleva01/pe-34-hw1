
/*
Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

Переменная let - видна только в рамках блока (в фигруных скобках {}), в котором объявлена, 
ее значение можно менять. Эта переменная видна только после ее обьявления.

Переменная  const - значение переменной не изменяемо, т.е значание указывается только один раз. 
В остальном такая же, как и let

Переменная var - имеет глобальную область видимости, это значит, 
что к ней можно обратиться в любой области кода и переписать ее. 
Почему объявлять переменную через var считается плохим тоном: 
К ней можно обратиться из любой области кода и изменить ее, получив бедосю с кодом и долго искать где же ошибка.
 */



let nameOfUser
let ageOfUser


do{ nameOfUser = prompt("Введите ваше имя" ,[nameOfUser])

} while (nameOfUser === "" || !isNaN(nameOfUser) || !nameOfUser )

do{ ageOfUser = parseInt (prompt("Введите ваш возраст" ,[ageOfUser]))

} while (isNaN(ageOfUser))





if (ageOfUser < 18) {
    alert("You are not allowed to visit this website")

} else if (ageOfUser >= 18 && ageOfUser <= 22) {
    alert("Welcome," + " " + nameOfUser)

} else if (confirm("Are you sure you want to continue?")) {
    alert("Welcome," + " " + nameOfUser)

} else {
    alert("You are not allowed to visit this website")
}